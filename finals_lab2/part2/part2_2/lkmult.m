function [dx, dy] = lkmult(I1w, I2w, epsilon, rho, scales)

Gp = fspecial('gaussian', ceil(3*3)*2+1, 3);
I1(1) = {I1w};
I2(1) = {I2w};

%Generate the gaussian pyramid
for i=1:scales-1
    T1 = cell2mat(I1(i));
    T2 = cell2mat(I2(i));
    T1 = imfilter(T1, Gp, 'conv');
    T2 = imfilter(T2, Gp, 'conv');
    I1(i+1) = {impyramid(T1,'reduce');};
    I2(i+1) = {impyramid(T2,'reduce');};
end

%Implement the one-scale algorithm to each level, starting at the top of the pyramid
%T1 = cell2mat(I1(scales+1));
%T2 = cell2mat(I2(scales+1));

[height, width] = size(T1);
dx_i = ones(height,width);                                                    % initialize dx and dy to one (original estimation)
dy_i = ones(height,width); 
%dx_i_size = size(dx_i)
%dy_i_size = size(dy_i)
    
for i=scales:-1:1
    T1 = cell2mat(I1(i));
    T2 = cell2mat(I2(i));
    dx_i = imresize(dx_i, [size(T1,1), size(T1,2)]);
	dy_i = imresize(dy_i, [size(T1,1), size(T1,2)]);
    %size_T1 = size(T1)
    %size_T2 = size(T2)
    [height, width] = size(T1);
    [dx_i, dy_i] = lk(T1, T2, epsilon, rho, dx_i, dy_i, height, width);                              % Lucas-Kanade
end

dx = dx_i;
dy = dy_i;
end