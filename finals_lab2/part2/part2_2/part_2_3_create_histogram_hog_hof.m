clear

%load('descs_hog_hof_gabor_cell.mat');
load('descs_hog_hof_harris_cell.mat');

all_feat=[];
for i=1:9
    all_feat = [all_feat;de{i}];
end
disp ('all_feat_done');
%

%kmeans
[idx,C] = kmeans(all_feat,1000);    % apply kmeans to all the features
disp('kmeans done');
 
hist = zeros(9, 1000);

for i=1:9
    distance = pdist2(de{i},C);
    [min_dist,min_id] = min(distance,[],2);
    hist(i,:) = histcounts(min_id,1000);
end
 
save('HOF_HOG_Harris_histogram', 'hist');