function [dx,dy] =lk(I1w, I2w, epsilon, rho, dx_i, dy_i, height, width)

max_iterations = 15;                                            
Gp = fspecial('gaussian',2*ceil(rho*3)+1,rho);  %Create the windowing function
iterations = 0;  %iterations count to stop running lk 

%The new apx displacement
dx_i_new = zeros(height, width);
dy_i_new = zeros(height, width);

%xreiazontai ola ayta mesa sto while?
while 1 
    iterations = iterations + 1;
    %fprintf('Iteration no: %d\n',iterations) 
    
    %Create grid to access all pixels 
    [x_0,y_0] = meshgrid(1:size(I1w,2),1:size(I1w,1));
    
    %Find the apx value of I1w at non-integer pixels x+di by interpolating,
    %we use it in eq. 4
    WarpI1 = interp2(I1w, x_0+dx_i, y_0+dy_i,'linear', 0); 
    %Do the same for the partial derivatives
    [WarpI1x, WarpI1y] = gradient(I1w);
    WarpI1x = interp2(WarpI1x, x_0+dx_i, y_0+dy_i, 'linear', 0);
    WarpI1y = interp2(WarpI1y, x_0+dx_i, y_0+dy_i, 'linear', 0);
    
    %eq. 7: E(x) = I_n(x) - I_n-1(x+di)
    E = I2w - WarpI1;                                                           
    
    %The two components of matrix A, given from equation 6
    %eq. 6
    A1 = WarpI1x;                                                            
    A2 = WarpI1y;
    
    % gradually compute the elements of matrix H (first matrix in eq. 5, the one to be inversed)
    A1_sq = A1.^2;
    A2_sq = A2.^2;
    A12 = A1.*A2;
    
    %We call v the second matrix of multiplication for equation 5
    v1 = imfilter(A1.*E, Gp);                                                  
    v2 = imfilter(A2.*E, Gp);
    
    %All four parts of H (eq. 5), without the epsilon
    H11 = imfilter(A1_sq, Gp) + epsilon*ones(height, width);                                              
    H12 = imfilter(A12, Gp);     
    H22 = imfilter(A2_sq, Gp) + epsilon*ones(height, width);
    H21 = H12;
    
    %Calculation of ux=Hinv*v1 and ux=Hinv*v1 without double for on each
    %pixel (x,y).
    det = H22 .* H11 - H12 .* H12;
    ux = (H11.*v1 + H12.*v2) ./ det;
    uy = (H21.*v1 + H22.*v2) ./ det;
    
    %d_i+1 = d_i + u
    dx_i = dx_i + ux;                                    
    dy_i = dy_i + uy;
    
    if  iterations >= max_iterations 
       break;  
    end
    
    %Check for convervegence using square loss

    if (immse(reshape(dx_i_new,[1,width*height]),reshape(dx_i,[1,width*height]))<0.002 ...
        && immse(reshape(dy_i_new,[1,width*height]),reshape(dy_i,[1,width*height]))<0.002) 
        break;
    end
    %{
    if (sum(abs(reshape(dx_i_new,[1,width*height])-reshape(dx_i,[1,width*height])))<0.002 ...
        && sum(abs(reshape(dy_i_new,[1,width*height])-reshape(dy_i,[1,width*height])))<0.002) 
        break;
    end
    %}
  
end
dx = dx_i;
dy = dy_i;
    
end
    