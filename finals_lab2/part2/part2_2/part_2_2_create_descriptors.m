clear
clc

G_x=load('Data/Gradient/derx_boxing_25.mat');
G_y=load('Data/Gradient/dery_boxing_25.mat');
a= load('Data/Int_points_gabor/interest_points_gabor_boxing_25.mat');

Gx_cell = G_x.dx;
Gy_cell = G_y.dy;
points = a.interest_points;

cube_size=zeros(1,3);
cube_size(1:2) = size(Gx_cell{1});
cube_size(3) = size(Gx_cell,2);

Gx=zeros(cube_size);
Gy=zeros(cube_size);

for i=1:cube_size(3)
    Gx(:,:,i) = Gx_cell{i};
    Gy(:,:,i) = Gy_cell{i};
end

n=4;
m=3;
nbins = 4;
scale =4;

number_of_points = size(points, 1);

desc=zeros(number_of_points, n*m*nbins);
for i=1:number_of_points
    
    x = points(i,2);
    y= points(i,1);
    z= points(i,4);
    
    if (x<=120-4)&&(y<=160-4)&&(z<=199-4)
        area_Gx = Gx(x:x+4,y:y+4,z:z+4);
        area_Gy = Gy(x:x+4,y:y+4,z:z+4);
    elseif (x>120-4)&&(y>160-4)&&(z>199-4)
        area_Gx = Gx(x:end,y:end,z:end);
        area_Gy = Gy(x:end,y:end,z:end);
    elseif (x>120-4)&&(y>160-4)
        area_Gx = Gx(x:end,y:end,z:z+4);
        area_Gy = Gy(x:end,y:end,z:z+4);
    elseif (x>120-4)&&(z>199-4)
         area_Gx = Gx(x:end,y:y+4,z:end);
         area_Gy = Gy(x:end,y:y+4,z:end);
    elseif (y>160-4)&&(z>199-4)
         area_Gx = Gx(x:x+4,y:end,z:end);
         area_Gy = Gy(x:x+4,y:end,z:end);
    elseif(x>120-4)
        area_Gx = Gx(x:end,y:y+4,z:z+4);
        area_Gy = Gy(x:end,y:y+4,z:z+4);
    elseif(y>160-4)
        area_Gx = Gx(x:x+4,y:end,z:z+4);
        area_Gy = Gy(x:x+4,y:end,z:z+4);
    elseif(z>199-4)
        area_Gx = Gx(x:x+4,y:y+4,z:end);
        area_Gy = Gy(x:x+4,y:y+4,z:end);
    end
        
    desc(i,:)=OrientationHistogram(area_Gx,area_Gy,nbins,[n m]);
end

save('desc_boxing_25','desc')
