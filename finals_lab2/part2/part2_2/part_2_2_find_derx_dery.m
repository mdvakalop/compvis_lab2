clear
clc

video = readVideo('material/samples/walking/person20_walking_d3_uncomp.avi', 200, 0);
[x, y, number_of_frames] = size(video);       

for i = 1:number_of_frames
    I = im2double(video(:,:,i));     %Take 2 consecutive frames
    
    maxI = max(max(I));              %Normalize the values to [0,1]
    I = I/maxI;
    
    [dx{i}, dy{i}] = imgradientxy(I);
end

save('derx_walking_20', 'dx');
save('dery_walking_20', 'dy');
