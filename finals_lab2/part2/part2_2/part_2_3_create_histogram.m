clear
clc

%Did the following procedure for each of HOG/HOF - Gabor/Harris

name{1} =('HOF_Harris/HOF_Harris_desc_boxing_16.mat');
name{2} =('HOF_Harris/HOF_Harris_desc_boxing_21.mat');
name{3} =('HOF_Harris/HOF_Harris_desc_boxing_25.mat');
name{4} =('HOF_Harris/HOF_Harris_desc_running_09.mat');
name{5} =('HOF_Harris/HOF_Harris_desc_running_15.mat');
name{6}= ('HOF_Harris/HOF_Harris_desc_running_23.mat');
name{7}= ('HOF_Harris/HOF_Harris_desc_walking_07.mat');
name{8}= ('HOF_Harris/HOF_Harris_desc_walking_14.mat');
name{9}= ('HOF_Harris/HOF_Harris_desc_walking_20.mat');

%Concatenate descriptors from all different videos
all_features = [];
descriptors = cell(1,9);

for i = 1:9
    descriptor = load(name{i});
    a = descriptor.desc;
    descriptors{i} = a;
    all_features = [all_features ; descriptors{i}];
end

disp('concatenated')


%Kmeans - Classify all extracted features
[idx, C] = kmeans(all_features, 1000); %Apply kmeans to all the features
disp('kmeans done');
 
hist = zeros(9, 1000);

%For each video, calculates its histogram according to all
%descriptor-centers from kmeans.
for i = 1:9
    distance = pdist2(descriptors{i}, C);
    [min_dist, min_id] = min(distance, [], 2);
    %Bag of words for kmenas centers
    hist(i,:) = histcounts(min_id, 1000);
end

save('HOF_Harris_histogram', 'hist');