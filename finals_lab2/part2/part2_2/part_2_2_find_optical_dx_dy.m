clear all

video = readVideo('../samples/boxing/person21_boxing_d1_uncomp.avi', 200, 0);
[x, y, number_of_frames] = size(video);

%parameters
multiscale_lk = 0;  %Set 1 if you want to run with Multiscale Lucas Kanade
rho = 6;           
epsilon = 0.1;
scales = 5;     %If mulitscale lk

for i = 1:number_of_frames-1
    
    %Take 2 consecutive frames
    I1 = im2double(video(:, :, i));        
    I2 = im2double(video(:, :, i+1));
    
    %Normalize the values to [0,1]
    max1 = max(max(I1));           
    max2 = max(max(I2));
    I1 = I1/max1;
    I2 = I2/max2;
    
    %Multiscale L-K
    if multiscale_lk == 1
       [dx{i},dy{i}] = lkmult(I1, I2, epsilon, rho, scales); 
    else
        %Initialize dx and dy to one (original estimation)
        dx_i = ones(x,y);
        dy_i = ones(x,y); 

        [dx{i},dy{i}] = lk(I1, I2, epsilon, rho, dx_i, dy_i, x, y);
    end
    i
end

save('optical_dx_boxing_21', 'dx');
save('optical_dy_boxing_21', 'dy');