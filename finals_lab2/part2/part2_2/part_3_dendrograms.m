close all
clear all

load('HOF_HOG_Harris_histogram.mat');
hist_hog_hof_harris = hist;
load('HOF_HOG_Gabor_histogram.mat');  
hist_hog_hof_gabor = hist;

load('HOG_Harris_histogram.mat');
hist_hog_harris = hist;
load('HOF_Harris_histogram.mat');
hist_hof_harris = hist;

load('HOG_Gabor_histogram.mat');  
hist_hog_gabor = hist;
load('HOF_Gabor_histogram.mat');  
hist_hof_gabor = hist;


for i=1:9
    C_hog_hof_harris(i,:) = distChiSq(hist_hog_hof_harris(i,:), hist_hog_hof_harris);
    C_hog_hof_gabor(i,:) = distChiSq(hist_hog_hof_gabor(i,:), hist_hog_hof_gabor);
    
    C_hog_harris(i,:) = distChiSq(hist_hog_harris(i,:), hist_hog_harris);
    C_hog_gabor(i,:) = distChiSq(hist_hog_gabor(i,:), hist_hog_gabor);
    
    C_hof_harris(i,:) = distChiSq(hist_hof_harris(i,:), hist_hof_harris);
    C_hof_gabor(i,:) = distChiSq(hist_hof_gabor(i,:), hist_hof_gabor);
end

D_hog_hof_harris = linkage(C_hog_hof_harris);
figure;
dendrogram(D_hog_hof_harris);
title("HOG HOF Harris dendrogram")
print -djpeg dendrogram_harris_hof_hog.jpg

D_hog_hof_gabor = linkage(C_hog_hof_gabor);
figure; 
dendrogram(D_hog_hof_gabor);
title("HOG HOF Gabor dendrogram")
print -djpeg dendrogram_gabor_hof_hog.jpg

D_hof_harris = linkage(C_hof_harris);
figure;
dendrogram(D_hof_harris);
title("HOF Harris dendrogram")
print -djpeg dendrogram_harris_hof.jpg

D_hof_gabor = linkage(C_hof_gabor);
figure; 
dendrogram(D_hof_gabor);
title("HOF Gabor dendrogram")
print -djpeg dendrogram_gabor_hof.jpg

D_hog_harris = linkage(C_hog_harris);
figure;
dendrogram(D_hog_harris);
title("HOG Harris dendrogram")
print -djpeg dendrogram_harris_hog.jpg

D_hog_gabor = linkage(C_hog_gabor);
figure; 
dendrogram(D_hog_gabor);
title("HOG Gabor dendrogram")
print -djpeg dendrogram_gabor_hog.jpg



