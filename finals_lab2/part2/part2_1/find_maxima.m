function [points_of_interest] = find_maxima(H, points, sigma)
    
    [~, index] = sort(H(:), 'descend');
    [x, y, t, scale] = ind2sub(size(H), index);
    points_of_interest = [y x sigma*scale t]; 
    points_of_interest = points_of_interest(points_of_interest(:,4)<190 & points_of_interest(:,4)>10, :);
    points_of_interest = points_of_interest(1:points, 1:end); 
   
end
