clear all

%Parameters
sigma = 3;
taf = 1;
s = 1;
k = 0.001;

I = readVideo('../samples/walking/person20_walking_d3_uncomp.avi',200,0);
%I = readVideo('../samples/boxing/person25_boxing_d4_uncomp.avi', 200, 0);
%I = readVideo('../samples/running/person23_running_d3_uncomp.avi', 200, 0);
I = im2double(I);

%apx derivatives of image-matrix in every direction
x(:,1,1) = [-1 0 1]';  
y(1,:,1) = [-1 0 1];   
t(1,1,:) = [-1 0 1];   

gradient_x = convn(I, x, 'same');
gradient_y = convn(I, y, 'same');
gradient_t = convn(I, t, 'same');

%3D Harris matrix J
J_x = imgaussfilt3(gradient_x.^2, [sigma sigma taf]);
J_y = imgaussfilt3(gradient_y.^2, [sigma sigma taf]);
J_t = imgaussfilt3(gradient_t.^2, [sigma sigma taf]);
J_xy = imgaussfilt3(gradient_x .* gradient_y, [sigma sigma taf]);
J_xt = imgaussfilt3(gradient_x .* gradient_t, [sigma sigma taf]);
J_yt = imgaussfilt3(gradient_y .* gradient_t, [sigma sigma taf]);

%Criterion
H = J_x .* (J_y.*J_t - J_yt.^2) - J_xy .* (J_xy.*J_t - J_xt.*J_yt) + ...
    J_xt .* (J_xy.*J_yt - J_y.*J_xt) - k * (J_x + J_y + J_t).^3;


points_of_interest = find_maxima(H, 500, sigma);    
    
showDetection(I, points_of_interest);

save('harris_walking_20', 'points_of_interest');
