%% 1.2 Gabor detector
clear all

sigma = 2;
taf = 1;
w = 4/taf;
points = 500;

%% Spacial smoothing of each frame using gauss kernel with deviation sigma
%I = readVideo('../samples/walking/person20_walking_d3_uncomp.avi', 200, 0);
%I = readVideo('../samples/running/person23_running_d3_uncomp.avi', 200, 0);
I = readVideo('../samples/boxing/person21_boxing_d1_uncomp.avi', 200, 0);
I = im2double(I);
Is = imgaussfilt(I, sigma);

%% Construction of Gabor filters
t = linspace(-2*taf, 2*taf, 10);                            
h_ev = -cos(2*pi*t*w) .* exp(-t.^2 ./ (2*taf.^2));
h_ev = h_ev / sum(h_ev);
h_od = -sin(2*pi*t*w) .* exp(-t.^2 ./ (2*taf.^2));
h_od = h_od / sum(h_od);

%% Gabor criterion
H = convn(Is, h_ev, 'valid').^2 + convn(Is, h_od, 'valid').^2;

points_of_interest = find_maxima(H, points, sigma);
showDetection(I, points_of_interest)

save('gabor_boxing_21', 'points_of_interest');