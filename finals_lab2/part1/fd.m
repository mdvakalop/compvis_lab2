function [x , y , width , height] = fd(I1, mu, sigma_mat)

cbcr1 = RGB2cbcr(I1);
rows = size(cbcr1, 1);
[im_x, im_y, z] = size(I1);
Prob_image = zeros(im_x, im_y);

for row = 1:rows
    Prob_image(row) = mvnpdf([(cbcr1(row, 1)) (cbcr1(row, 2))], mu, sigma_mat);
end
%Normalize probability in [0, 1]
Prob_image = Prob_image / max(Prob_image(:));

%Keep the pixels over a threshold (0.3) and smooth large areas
P = Prob_image > 0.3;
Popen = imopen(P, strel('disk', 3));
Popen_close = imclose(Popen, strel('disk', 25));

figure;
subplot(2,2,1); imshow(P); title('Probability image of frame 1');
subplot(2,2,2); imshow(Popen); title('Opening filtering');
subplot(2,2,3); imshow(Popen_close); title('Closing filtering');
    
[Lopen_close, ~] = bwlabel(Popen_close, 4);

%Find the conected component with the biggest cardinality 
%(biggest derma-area)
cardinalities = arrayfun(@(i) sum(Lopen_close(:) == i), [1 2 3]);
[~, max_card_index] = max(cardinalities);
%Keep only this component
Lopen_close = Lopen_close == max_card_index;

Bounding_Box = regionprops(Lopen_close, 'BoundingBox');
object_specs = Bounding_Box.BoundingBox ;          

%Small increment of the bounding box
x = object_specs(1) - 10;                                  
y = object_specs(2) - 10;                                  
width = object_specs(3) + 20;                             
height = object_specs(4) + 20; 

figure;                                             
imshow(I1);
rectangle('Position', [x y width height], 'EdgeColor', 'g')
    
end



