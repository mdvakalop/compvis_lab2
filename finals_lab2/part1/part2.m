%Load the frames
for i=1:72
    G{i} = imread(strcat('GreekSignLanguage/GreekSignLanguage/GSLframes/', num2str(i), '.png'));
end

%load x0, y0, width and height of Bounding Box from part 1                                                    
load('BoundingBox.mat');

x1 = floor(x0); %x0, y0 are doubles, we turn them into integers
y1 = floor(y0); %in order to be able to access the pixels

%Bounding box for the first frame from part 1
figure; 
imshow(G{1})
rectangle('Position',[x0 y0 width height],'EdgeColor','g')

%Parameters for Lucas Kanade
multiscale_lk = 0;  %Set 1 if you want to run with Multiscale Lucas Kanade
rho = 3;           
epsilon = 0.1;
scales = 3;  %If mulitscale lk

%Parameter for mean optical flow
thresh = 0.3;

%Parameters for displays
vis_flow = 0;    %set =1 to see flow and save figs
vis_frames = 1;  %set=1 to see and save frames with bounding box  
vis_energy = 0;  %set=1 to save and see energy 

%% Complete algorithm using (Multiscale) Lucas-Kanade for every two conecutive frames

for i = 1:70
    
    %Read 2 consecutive frames 
    I1 = G{i};
    I2 = G{i+1};

    %Keep only the gray scale bounding box
    I1_box = double(rgb2gray(I1(y1:y1+height-1,x1:x1+width-1,:)));
    I2_box = double(rgb2gray(I2(y1:y1+height-1,x1:x1+width-1,:)));
    
    %Normalize boxes, pixel values set in [0,1] 
    max1 = max(I1_box(:));
    max2 = max(I2_box(:));
    I1_box = I1_box / max1;
    I2_box = I2_box / max2; 
    
    %Initialize dx and dy to one for each pixel (original estimation)
    dx_i = ones(height,width);                                                
    dy_i = ones(height,width); 
    
    if multiscale_lk
        %Multi-scale Lucas Kanade algorithm
        [dx, dy] = lkmult(I1_box, I2_box, epsilon, rho, scales);
    else
        %Simple Lucas-Kanade algorithm
        [dx, dy] = lk(I1_box, I2_box, epsilon, rho, dx_i, dy_i, height, width); 
    end
    
    %% Displays and saves
    
    if vis_flow == 1
        dx_r = imresize(dx,0.3);
        dy_r = imresize(dy,0.3);
        %figure;
        quiver(-dx_r, -dy_r)
        title(strcat('Optical Flow of frame ', num2str(i)))
        eval(['print -dpng Optical_flow_frame_' num2str(i) '.png']);       
    end
    
    %Calculate the displacement of the bounding box
	%using the vector field's Energy
    [displ_x, displ_y, energy] = displ(-dx , -dy, thresh);

    %Move the box
    x1 = x1 + displ_x;
    y1 = y1 + displ_y;
    
    if vis_energy == 1 
        %figure;
        imshow(energy,[])
        title (strcat('Energy of frame ',num2str(i)))
        eval(['print -dpng Energy_frame_' num2str(i) '.png']);
    end
    
   if vis_frames == 1
        %figure;
        imshow(I2, [])
        rectangle('Position', [x1 y1 width height], 'EdgeColor', 'g')
        title (strcat('Face position in frame ', num2str(i)))
        eval(['print -dpng Face_' num2str(i) '.png']);
    end
 
end