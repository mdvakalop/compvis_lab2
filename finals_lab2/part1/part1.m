load('skinSamplesRGB.mat');
cbcr_skins = RGB2cbcr(skinSamplesRGB);
cb = cbcr_skins(:, 1);
cr = cbcr_skins(:, 2);
mu = mean(cbcr_skins);
sigma_mat = cov(cbcr_skins);

I1 = imread('GreekSignLanguage/GreekSignLanguage/GSLframes/1.png');
[x0, y0, width, height] = fd(I1, mu, sigma_mat);

save('BoundingBox.mat', 'x0', 'y0', 'width', 'height');