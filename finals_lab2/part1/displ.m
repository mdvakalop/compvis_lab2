function [displ_x, displ_y, Energy] = displ(dx, dy, thresh)

    Energy = dx.^2 + dy.^2;      % calculate the energy for each pixel
    maximum = max(Energy(:));     % find maximum energy

    [height, width] = size(dx); 
    %initialize the sums
    sumx = 0;
    sumy = 0;
    count = 0;

    for x = 1:width
        for y = 1:height
            if Energy(y,x)/maximum >= thresh  % if the energy exceeds the threshold
                sumx = dx(y,x) + sumx;     % then add the dx    
                sumy = dy(y,x) + sumy;     % and the dy
                count = count + 1;       % count is for calculating the mean value
            end     
        end
    end

    displ_x = ceil((sumx/count));  % displacement in x is the mean value of the displacements exceeding the threshold
    displ_y = ceil((sumy/count));  % same for displacement for y
end
