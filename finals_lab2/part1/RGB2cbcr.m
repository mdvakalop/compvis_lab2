function cbcr = RGB2cbcr (rgb_im)

    x=size(rgb_im,1);                   % how many rows in original
    y=size(rgb_im,2);                   % how many columns in original
    ycbcr = rgb2ycbcr(rgb_im);          % convert to YCbCr
    ycbcr = reshape(ycbcr,[(x*y) 3]);   % reshape to get MxNx3
    cbcr = ycbcr(:,2:3);                % keep only Cb and Cr
    cbcr = double(cbcr);                % turn uint8 to double because it's necessary later

end